var irc = require('irc');
var amqp = require('amqplib/callback_api');

var proxies = {}; 		// mapa de proxys
var amqp_conn;    		// Conexão AMQP
var amqp_ch;      		// Canal AMQP
var irc_clients = {};	// Lista de Clients

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;
		
		inicializar();
	});
});

function inicializar() {
	receberDoCliente("registro_conexao", function (msg) {
		console.log('irc-proxy.js: recebeu registro de conexão');
		
		var id       = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;
		
		irc_clients[id] = new irc.Client(
			servidor, 
			nick,
			{channels: [canal]}
		);		
		
		irc_clients[id].addListener('message'+canal, function (from, message) {
			console.log(from + ' => '+ canal +': ' + message);
			if(message.trim().split(" ")[0].toUpperCase() == "/PRIVMSG"){
				message = message.trim().split(":")[1];
			}
			enviarParaCliente(id, {
				"timestamp": Date.now(), 
			   	"nick": from,
			 	"msg": message
			});
		});

		irc_clients[id].addListener('ctcp', function(from, to, text, type, message){
								console.log('from: ' + from);
								console.log('to: ' + to);
								console.log('text: ' + text);
								console.log('type: ' + type);
								console.log('message: ' + message);
				
								enviarParaCliente(id, {
											"timestamp": Date.now(), 
											"nick": from,
											"msg": message
								});
							}
		);

		irc_clients[id].addListener('raw', function(message) {
		  	console.log('user: '+ message.nick);
		  	console.log('command: '+ message.command);
		  	console.log('args: '+ JSON.stringify(message.args));
		  	console.log('/////////////////////////////////////////');
			if(message.command.toUpperCase() == 'PRIVMSG') {
				console.log('privmsg recebido: Ok');
				var from = message.nick;
				var msg = message.args[1].trim();
				enviarParaCliente(id, {
							"timestamp": Date.now(), 
							"nick": from,
							"msg": msg
							});
			}
		});
		
		irc_clients[id].addListener('error', function(message) {
			console.log('error: ', message);
		});
		
		proxies[id] = irc_clients[id];
	});
	
	receberDoCliente("gravar_mensagem", function (msg) {
		console.log("gravar msg: " + JSON.stringify(msg));
		if(msg.msg.trim().split(" ")[0].toUpperCase() == "/PRIVMSG"){
			var dest = msg.msg.split(" ")[1];
			var mensg = msg.msg.split(":")[1];
			console.log("enviou privmsg " + dest + " " + mensg);
			irc_clients[msg.id].say(dest, mensg);
		}
		else {
			irc_clients[msg.id].say(msg.canal, msg.msg);
		}

	});
}

function receberDoCliente (canal, callback) {
	amqp_ch.assertQueue(canal, {durable: false});
	
	console.log(" [irc] Aguardando mensagens no canal: " + canal);
	
	amqp_ch.consume(canal, function(msg) {
		console.log(" [irc] Recebido: %s", msg.content.toString());
		callback(JSON.parse(msg.content.toString()));
	}, {noAck: true});
}

function enviarParaCliente (id, msg) {
	msg = new Buffer(JSON.stringify(msg));
	
	amqp_ch.assertQueue("user_"+id, {durable: false});
	amqp_ch.sendToQueue("user_"+id, msg);
	
	console.log(" [irc] Enviado para o ID " + id + ": " + msg);
}





# Projeto 3

Protótipo do projeto 3 das disciplinas de Aplicações Distribuídas (SI) e Sistemas Distribuídos (EC) do semestre 2017-1

## Sumário

* [Membros] (https://gitlab.com/ad-si-2018-1/p3-g1/tree/master/#1-lista-de-membros-do-grupo)
* [Introdução] (https://gitlab.com/ad-si-2018-1/p3-g1/tree/master/#2-introdu%C3%A7%C3%A3o)
* [Requisitos] (https://gitlab.com/ad-si-2018-1/p3-g1/tree/master/#3-requisitos)
* [Funcionamento] (https://gitlab.com/ad-si-2018-1/p3-g1/tree/master/#4-funcionamento)
* [Execução dos Arquivos] (https://gitlab.com/ad-si-2018-1/p3-g1/tree/master/#5-execu%C3%A7%C3%A3o-dos-arquivos)

***

### Lista de membros do grupo
**Nome** | **Função** |
:---:|:---------:
Ygor Santos| Líder e Desenvolvedor |
Rhandy Mendes Ferreira| Desenvolvedor |
Victor Murilo| Documentador |
Pedro Andrade| Documentador |

### Introdução

Este repositório foi criado para que seja feito o terceiro projeto do grupo 1, da disciplina Aplicações Distríbuidas da Universidade Federal de Goiás, ministrado pelo professor Marcelo Akira.

### Requisitos



### Funcionamento



### Execução dos arquivos
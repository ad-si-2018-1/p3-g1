/*
	Envia a mensagem para o servidor.
*/
function enviarParaServidor (comando, msg, amqp_ch) {
	msg = new Buffer(JSON.stringify(msg));
	
	amqp_ch.assertQueue(comando, {durable: false});
	amqp_ch.sendToQueue(comando, msg);
	console.log(" [app] Enviado: %s", msg);
}

/*
	Recebe a mensagem do servidor.
*/
function receberDoServidor (id, amqp_ch, callback) {
	amqp_ch.assertQueue("user_"+id, {durable: false});
	
	console.log(" [app] Aguardando mensagens para:  "+ id);
	
	amqp_ch.consume("user_"+id, function(msg) {
		console.log(" [app] ID "+id+" Recebido: "+msg.content.toString());
		callback(id, JSON.parse(msg.content.toString()));
	}, {noAck: true});
}

module.exports = {
	enviarParaServidor : enviarParaServidor,
	receberDoServidor : receberDoServidor
};